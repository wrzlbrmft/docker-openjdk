# docker-openjdk

Customized OpenJDK (Amazon Corretto)

Based on https://hub.docker.com/_/amazoncorretto . Upgraded and with some extra packages.

```
docker pull wrzlbrmft/openjdk:<version>
```

See also:

  * https://openjdk.org/
  * https://aws.amazon.com/corretto
  * https://hub.docker.com/r/wrzlbrmft/openjdk/tags

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
