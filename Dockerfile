ARG TAG=21-alpine
FROM amazoncorretto:${TAG}

RUN apk upgrade --no-cache \
    && apk add --no-cache \
        netcat-openbsd
